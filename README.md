#CodeCloud.WeChatSDK
基于.NET FrameWork4.0 开发的微信SDK，主要封装了微信公众平台API，实现快捷方便的项目开发。
部分代码参考了来源于书籍：《微信公众平台企业开发实战》。
[官方网站——码云网：http://www.codecloud.wang/](http://www.codecloud.wang/)